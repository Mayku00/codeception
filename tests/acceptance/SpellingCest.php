<?php

use Codeception\Util\Locator;
class SpellingCest
{
    public function tryToTest(AcceptanceTester $I)
    {
		$I->amOnPage('/');
		$I->seeInTitle('trivago.es');
		$I->seeElement('#horus-querytext');
		$I->see('Buscar');
		$I->fillField('#horus-querytext','Mdrid');
		$I->wait(1);
		$I->click('Buscar');
		$I->wait(1);
		$I->seeInTitle('Hoteles en Madrid');
		$I->wait(2);
		$I->click('ol.hotellist li:first-child button.btn--deal');
		$I->executeInSelenium(function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
			$handles = $webdriver->getWindowHandles();
			$lastWindow = end($handles);
			$webdriver->switchTo()->window($lastWindow);
			});
		$I->wait(5);
		$I->see('NH Madrid Alonso Martínez');
	}
}
