<?php


class StarTwoCest
{
    public function tryToTest(AcceptanceTester $I)
    {
		$I->amOnPage('/');
		$I->seeInTitle('trivago.es');
		$I->seeElement('#horus-querytext');
		$I->see('Buscar');
		$I->fillField('#horus-querytext','Sercotel Infanta Isabel');
		$I->click('Buscar');
		$I->wait(1);
		$I->seeInTitle('Sercotel');
		$I->click('.df_overlay_close_wrap.overlay__close');
		$I->click('.toolbar-list__item.toolbar-list__item--stars > button');
		$I->wait(1);
		$I->click('label + label + label > input');
		$I->wait(1);
		$I->click('Listo');
		$I->See('Sercotel Infanta Isabel','ol.hotellist li:first-child h3');
		$I->wait(2);
		$I->click('ol.hotellist li:first-child button.btn--deal');
		$I->executeInSelenium(function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
			$handles = $webdriver->getWindowHandles();
			$lastWindow = end($handles);
			$webdriver->switchTo()->window($lastWindow);
			});
		$I->wait(6);
		$I->see('Hotel Infanta Isabel');
    }
}

