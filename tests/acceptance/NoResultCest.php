<?php


class NoResultCest
{
	public function noResult(AcceptanceTester $I)
    {
		$I->amOnPage('/');
		$I->seeInTitle('trivago.es');
		$I->seeElement('#horus-querytext');
		$I->see('Buscar');
		$I->fillField('#horus-querytext','xxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
		$I->wait(1);
		$I->click('Buscar');
		$I->wait(1);
		$I->see('Ningún resultado para "xxxxxxxxxxxxxxxxxxxxxxxxxxxxx"');
		$I->dontSeeElement('ol.hotellist');
    }
}
