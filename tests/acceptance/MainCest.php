<?php


class MainCest
{
    public function tryToTest(AcceptanceTester $I)
    {
		$I->amOnPage('/');
		$I->seeInTitle('trivago.es');
		$I->seeElement('#horus-querytext');
		$I->see('Buscar');
		$I->fillField('#horus-querytext','España');
		$I->click('Buscar');
		$I->wait(1);
		$I->seeInTitle('Hoteles en España');
		$I->click('.df_overlay_close_wrap.overlay__close');
		$I->seeElement('.toolbar-list__item.toolbar-list__item--stars > button');
		$I->click('.toolbar-list__item.toolbar-list__item--stars > button');
		$I->wait(1);
		$I->click('Listo');
		$I->wait(1);
		$I->seeElement('ol.hotellist li:first-child h3');
		$I->seeElement('ol.hotellist li:first-child button.btn--deal');
    }
}
