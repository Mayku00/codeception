<?php


class StarOneCest
{
    public function tryToTest(AcceptanceTester $I)
    {
		$I->amOnPage('/');
		$I->seeInTitle('trivago.es');
		$I->seeElement('#horus-querytext');
		$I->see('Buscar');
		$I->fillField('#horus-querytext','Sercotel Infanta Isabel');
		$I->click('Buscar');
		$I->wait(1);
		$I->seeInTitle('Sercotel');
		$I->click('.df_overlay_close_wrap.overlay__close');
		$I->click('.toolbar-list__item.toolbar-list__item--stars > button');
		$I->wait(1);
		$I->click('label:first-child > input');
		$I->wait(1);
		$I->click('Listo');
		$I->dontSee('Sercotel Infanta Isabel','ol.hotellist li:first-child h3');
    }
}
