# README #

# Pre-Requisites:

Install codeception: https://codeception.com/docs/modules/WebDriver
Install chromedriver: https://codeception.com/docs/modules/WebDriver
run chromedriver: chromedriver --url-base=/wd/hub


# Project:
copy test folder to codeception folder
 
# Structure:
 test/acceptance.suite.yml
 test/acceptance/MainCest.php
 				 /NoResultCest.php
				 /SpellingCest.php
				 /StarOneCest.php
				 /StarTwoCest.php
				 
# run:
php codecept run acceptance